\input{header.tex}
\input{metadata.tex}
\input{shortcuts.tex}

\author{
  Tim Kallage    \\    tim.kallage@tu-dortmund.de \and
  Christian Geister \\ christian.geister@tu-dortmund.de
}
\title{(\Versuchsnummer)\\\Versuchstitel}
\date{Durchführung: \Versuchsdatum}

\begin{document}
\maketitle
\thispagestyle{empty}
\vfill
\tableofcontents
\newpage

%------------------------------------------------
\section{Motivation}
%------------------------------------------------
In diesem Versuch wird optisches Pumpen benutzt, um die Hyperfeinstruktur
von Rubidiumatomen zu untersuchen. Daraus können dann die Landéschen
$g_\mathrm{F}$-Faktoren, sowie Kernspins für \textsuperscript{85}Rb
und \textsuperscript{87}Rb ermittelt werden.

%------------------------------------------------
\section{Theoretische Grundlagen}
%------------------------------------------------
\subsection{Aufspaltung der Energieniveaus}
%------------------------------------------------
Rubidium besitzt als Alkalimetall nur ein Valenzelektron.
Alle anderen Schalen sind vollständig besetzt und besitzen deshalb insgesamt keinen
Bahndrehimpuls. Die Energieniveaus des Valenzelektrons, die durch die Hauptquantenzahl
$n$ gekennzeichnet sind, werden durch die Wechselwirkung des Elektronenspins
$S = \frac12$, welcher das magnetische Moment des Elektrons erzeugt, mit dem Bahndrehimpuls
aufgespalten. Dieses Phänomen wird als Feinstrukturaufspaltung bezeichnet.
Das magnetische Moment der Elektronenhülle $\vec \mu_J$ ist dabei gegeben durch
\begin{equation}
  \vec \mu_J = -g_J\mu_\mathrm{B}\vec J,
\end{equation}
mit dem Bohrschen Magnetron $\mu_\mathrm{B}$ und dem Landé-Faktor $g_J$, der
berücksichtigt, dass das Gesamtdrehmoment um die $\vec J$-Richtung präzediert und daher
nur dessen zu $\vec J$ parallele Komponente beiträgt.
\begin{equation}
  g_J = \frac{\num{3,0023}J(J+1) + \num{1,0023}\left\{S(S+1) - L(L+1)\right\}}{2J(J+1)}
\end{equation}
Wenn auch der Spin des Atomkerns betrachtet wird, so erhält man eine weitere Aufspaltung
der Energieniveaus, die Hyperfeinstruktur.
Diese entsteht durch die vektorielle Kopplung des Gesamtdrehimpulses der Elektronenhülle $\vec J$
(bei Alkali-Atomen nur bestehend aus dem Gesamtdrehimpuls des Valenzelektrons) an den
Drehimpuls $\vec I$ des Kerns zum Gesamtdrehimpuls des Kerns
\begin{equation}
  \vec F = \vec J + \vec I
\end{equation}
Die Niveaus werden durch die Quantenzahl $F$ unterschieden, die Werte im Abstand eins
zwischen $|I-J|$ und $I+J$ annehmen kann, und damit $2J+1$ oder $2I+1$ Werte annehmen kann,
jenachdem welche Quantenzahl $I$ oder $J$ kleiner ist.
Als letzte Aufspaltung wird hier der Zeeman-Effekt beschrieben.
Dieser entsteht beim anlegen eines kleinen, magnetischen Felds.
Für ein Hyperfeinstruktur mit der Quantenzahl $F$ entstehen $2F+1$ Zeeman-Niveaus mit den
Quantenzahlen $M_F \in \{-F,\dots,F\}$.
Die Energieaufspaltung zwischen diesen Niveaus beträgt
\begin{equation}
  \Delta E = g_F\mu_\mathrm{B}B .
\end{equation}
Die beschriebenen Aufspaltungen sind schematisch in Abb.~\ref{fig:Aufspaltungen} dargestellt.
\begin{figure}[tb]
  \centering
  \includegraphics[width=.8\textwidth]{Aufspaltung}
  \caption{Die Aufspaltungen der Energieniveaus eines Alkaliatoms mit $I = \frac32$
  für je einen Zustand mit $L = \{0, 1\}$ (Aufspaltungen nicht maßstäblich)\cite{Anleitung}.}
  \label{fig:Aufspaltungen}
\end{figure}
Das magnetische Moment eines Elektrons, das Bohrsche Magnetron
$\mu_\mathrm{B} = \frac{e\hbar}{2m_e}$,
ist sehr viel größer als das Kernmagnetron $\mu_\mathrm{K} = \frac{e\hbar}{2m_p}$, da die Masse
des Elektrons $m_e$ um etwa den Faktor 2000 kleiner ist als die Protonmasse $m_p$.
Daher kann in guter Näherung der Einfluss des Kernspins auf das magnetische Moment des Atoms
vernachlässigt werden.
Mit weiteren Überlegungen ergibt sich der Landé-Faktor des Kerns zu
\begin{equation}
  g_F = g_J \frac{F(F+1)+J(J+1)-I(I+1)}{2F(F+1)} .
\end{equation}
%------------------------------------------------
\subsection{Das Prinzip des optischen Pumpens}
%------------------------------------------------
Im thermischen Gleichgewicht ist das Verhältnis zweier Energieniveaus der äußeren Elektronen
mit den Energien $W_{1,2}$ und den Entartungsgraden $g_{1,2}$ gegeben durch
\begin{equation}
  \frac{N_2}{N_1} = \frac{g_2}{g_1}\frac{\exp(-W_2/k_\mathrm{B}T)}{\exp(-W_1/k_\mathrm{B}T)} ,
\end{equation}
für $W_2 > W_1$ gilt also $N_2 < N_1$.
Durch die richtige Anregung von bestimmten Zuständen kann $N_2 > N_1$, eine sogenannte
Inversion, erzeugt werden.
Das Einstrahlen von rechtszirkular polarisiertem Licht, dessen Frequenz dem Abstand der
Feinstrukturaufspaltung zwischen den $^2S_{\sfrac{1}{2}}$ und $^2P_{\sfrac{1}{2}}$ entspricht,
werden die Elektronen aus dem $^2S_{\sfrac{1}{2}}$ Zustand in den $^2P_{\sfrac{1}{2}}$ angeregt
($D_1$-Übergang, siehe Abb.~\ref{fig:Aufspaltungen}).
Da für rechtszirkular polarisiertes Licht allerdings die Auswahlregel $\Delta M_F = +1$
gilt und es keinen $^2P_{\sfrac{1}{2}}$ Zeeman-Zustand mit $M_F = +3$ gibt,
ist der Zustand mit den Quantenzahlen $L=0, F=2, M_F=+2$ dunkel (wenn ein Magnetfeld $B \neq 0$
angelegt ist), d.h. er absorbiert das einfallende Licht nicht.
Die angeregten Elektronen in den $^2P_{\sfrac{1}{2}}$-Zuständen regen sich nach sehr kurzer
Zeit wieder in einen der Zustände mit $L=0$ ab ($\tau \approx \SI{e-8}{\second}$), wodurch sich
nach kurzer Zeit ein Großteil der Elektronen im energetisch ungüstigeren, dunklen Niveau
sammeln.
Der zeitliche Verlauf des Pumpvorgangs kann mit einer Sättigungskurve der Form
\begin{equation}
  \frac{N_2}{N_1}(t) = a\left(1 - \exp\left(-\frac{t}{\tau}\right)\right) + b
\end{equation}
beschrieben werden. Der Parameter $\tau$ ist dabei charakteristisch für die Dauer
des Pumpvorgangs.
%------------------------------------------------
\subsection{Präzisionsmessung der Zeeman-Aufspaltung}
%------------------------------------------------
Der Übergang aus dem dunklen Zustand in einen energetisch güstigeren, nicht-dunklen
Zustand kann über zwei Arten erfolgen, mittels spontaner und induzierter Emission.
Spontane Emission kann für den Übergang zwischen zwei Zeeman-Niveaus vernachlässigt werden,
ganz im Gegensatz z.B. zum $D_1$-Übergang. Dies liegt an der kubischen Abhängigkeit
der Übergangswahrscheinlichkeit von der Energiedifferenz, die für die Zeeman-Niveaus
sehr viel kleiner als für Feinstruktur-Niveaus ist.
Der induzierte Übergang kann nun durch das Anlegen eines hochfrequenten RF-Feldes
erzeugt werden. Sobald das Magnetfeld den Wert
\begin{equation}
  B_m = \frac{4\pi m_e}{eg_J}\nu
\end{equation}
erreicht, wobei $\nu$ die Resonanzfrequenz ist,
wird das Entleeren des dunklen Zustandes induziert.
Dieser Vorgang kann detektiert werden, indem die Transparenz des Gases beobachtet wird.
Nach dem Pumpvorgang ist das Gas transparent, da keine Absorption des eingestrahlten Lichts
möglich ist. Mit dem Entleeren des dunklen Zustands nimmt die Transparenz ab, da durch
das Entleeren die nicht-dunklen Niveaus bevölkert werden.
Der typische Verlauf bei der Variation des Magnetfelds ist in Abb.~\ref{fig:Transparenz}
dargestellt.
\begin{figure}[tb]
  \centering
  \includegraphics[width=.5\textwidth]{Transparenz}
  \caption{Transparenz in Abhängigkeit vom angelegten Magnetfeld\cite{Anleitung}.}
  \label{fig:Transparenz}
\end{figure}
Bei $B=0$ ergibt sich ebenfalls keine Transparenz, da in diesem Fall die Energieniveaus
in $M_F$ entartetet sind und daher der Pumpvorgang in das dunkle Niveau nicht möglich ist.
%------------------------------------------------
\subsection{Quadratischer Zeeman-Effekt}
%------------------------------------------------
Die Zeeman-Aufspaltung erzeugt bei niedrigen Feldstärken äquidistante Niveaus.
Für größere magnetische Felder müssen allerdings weitere Terme, proportional
zu $B^2$ berücksichtigt werden, welche wiederum u.a. von der Quantenzahl $M_F$
abhängen.
Mit dem Abstand der Hyperfeinstrukturniveaus $\Delta E_\mathrm{Hy}$ ergibt sich
die Zeeman-Übergangsenergie für einen Übergang $M_F$ nach $M_F-1$ zu
\begin{equation}
  \Delta E = g_F\mu_\mathrm{B}B + (g_F\mu_\mathrm{B}B)^2\frac{1 - 2M_F}{\Delta E_\mathrm{Hy}}.
  \label{eqn:quad:zeeman}
\end{equation}
%------------------------------------------------
\subsection{Transiente Effekte}
%------------------------------------------------
Beim Einschalten des RF-Feldes auf der Resonanz können sogenannte transiente Effekte
beobachtet werden. Die Elektronen oszillieren durch eine Präzessionsbewegung zwischen
dem dunklen Zustand und jenem, zu dem der Übergang induziert wird.
Diese Oszillation spiegelt sich ebenfalls im zeitlichen Verlauf der Transmittivität
des Gases wieder und kann vermessen werden.
Das externe Magnetfeld $B_0$ steht mit der Larmorfrequenz $\omega_0$ über
das gyromagnetische Verhältnis $\gamma = \frac{\mu_\mathrm{B}g_S}{h}$ in Relation.
\begin{equation}
  \omega_0 = \gamma B_0
\end{equation}
In einem rotierenden Koordinatensystem präzediert der Atomgesamtspin $\vec F$
mit der Larmorfrequenz um das $B_{RF}$, womit sich die Periodendauer jener zu
\begin{equation}
  T = \frac{1}{\gamma B_{RF}}
\end{equation}
ergibt.
Für Unterschiedliche Isotope kann dann aus der Relation der Perioden das Verhältnis der
gyromagnetischen Verhältnisse errechnet werden
\begin{equation}
  \frac{T_1}{T_2} = \frac{\gamma_2}{\gamma_1} .
\end{equation}
%------------------------------------------------
\section{Versuchsaufbau}
%------------------------------------------------
\begin{figure}[tb]
  \centering
  \includegraphics[width=\textwidth]{Apparatur}
  \caption{Schematische Darstellung der verwendeten Apparatur\cite{Anleitung}.}
  \label{fig:Apparatur}
\end{figure}
Als Material wird in diesem Versuch das Alkalimetall Rubidium verwendet, welches
die oben vorausgesetzten Eigenschaften besitzt.
Für die gewünschte Anregung wird Licht mit der Wellenlänge des $D_1$-Übergangs von
Rubidium benötigt, welches rechtszirkular polarisiert ist.
Die nun beschriebene Apparatur ist in Abb.~\ref{fig:Apparatur} schematisch dargestellt.
Hierzu wird einfach eine Rubidiumdampflampe verwendet, hinter die zuerst eine kollimierende
Linse und dann ein Interferenzfilter geschaltet ist,
welches nur den $D_1$-Übergang transmittiert.
Durch einen Polfilter wird das Licht nun linear polarisiert und durch ein $\lambda/4$-Plättchen,
welches in einem Winkel von \SI{45}{\degree} zur Polarisationsrichtung in den
Strahlengang gesetzt wird, wird zirkular polarisiertes Licht erzeugt.
Der Strahl durchläuft nun die Dampfzelle, in der eine Heizung einen konstanten Rubidium-Gasdruck
erzeugt.
Der Rubidiumdampf besteht aus den Isotopen \textsuperscript{85}Rb und \textsuperscript{87}Rb.
Eine Sammellinse fokussiert das Licht auf einen Lichtdetektor, welcher über einen Verstärker
an ein Oszilloskop angeschlossen ist.
Die Dampfzelle ist von drei Helmholtz-Spulenpaaren umgeben, zwei horizontale und ein
vertikales Paar. Außerdem umgibt die Dampfzelle eine Spule, die das RF-Feld erzeugt.
Der Aufbau wird in Nord-Süd-Richtung ausgerichtet, sodass das Magnetfeld der Erde
mit den Möglichkeiten der einstellbaren Horizontal- und Vertikalfelder kompensiert werden kann.
Die Spulenpaare besitzen folgende Eigenschaften:
\begin{table}[!h]
\caption{Eigenschaften der verwendeten Helmholtz-Spulenpaaren.}
\label{tab:Spulen}
\centering
\begin{tabular}{lSS}
\toprule
Spule & {Radius $R$ [cm]} & {Windungszahl $N$} \\
\midrule
Modulationsfeld(Sweep-)spule & 16,39 & 11 \\
Horizontalfeldspule & 15,79 & 154 \\
Vertikalfeldspule & 11,735 & 20 \\
\bottomrule
\end{tabular}
\end{table}
%------------------------------------------------
\section{Messprogramm}
%------------------------------------------------
Nach der Justage werden nun zur Messung folgende Schritte durchgeführt:
\begin{enumerate}
  \item[a)]
    Während die Sweepspule ein Magnetfeld erzeugt, das in \SI{2}{Sekunden} von
    Null auf dessen Maximalwert variiert wird, zeigt sich auf dem Oszilloskop ein breiter
    Peak nach unten. Dieser wird durch Einstellen des vertikalen Magnetfeldes schmaler gemacht
    und so die Vertikalkomponente des Erdmagnetfelds kompensiert.
    Das vertikale Magnetfeld wird beim schmalsten Peak als Messwert genommen.
  \item[b)]
    Das RF-Feld wird nun eingeschaltet.
    Die Frequenz des RF-Feldes wird nun zwischen 100 und \SI{1000}{\kilo\hertz} in
    \SI{100}{\hertz}-Schritten variiert. Mit der Sweep- und der Horizontalfeldspule
    wird nun das Feld so eingestellt, dass die Resonanz getroffen wird.
    Dies ist an der Abnahme der durchgelassenen Lichtintensität zu erkennen.
    Die Werte der beiden Spulen werden notiert.
    Da zwei Rubidium-Isotope vorliegen, werden beide Resonanzen vermessen.
  \item[c)]
    Als nächstes wird die Frequenz des RF-Feldes auf \SI{100}{\hertz} eingestellt.
    Mit einem Rechteckspannungsgenerator wird das RF-Feld mit einer Frequenz von
    $\SI{5}{\hertz}$ ein- und wieder ausgeschaltet um transiente Effekte beobachten zu können.
    Außerdem wird die Amplitude des RF-Magnetfelds variiert, indem die Amplitude des
    HF-Funktionsgenerators zwischen \num{0,5} und \SI{5,0}{\volt} in \SI{0,5}{\volt}-Schritten
    verändert wird.
    Die Periode der Oszillation wird als Messwert für jede Amplitude genommen.
\end{enumerate}
%------------------------------------------------
\section{Auswertung}
%------------------------------------------------
\subsection{Vertikalkomponente des Erdmagnetfelds}
Der Peak wird minimal bei einer Stromstärke für das Vertikalfeld von \val{I_v}.
Mit der Formel für ein Helmholz-Spulenpaar
\begin{equation}
B = \mu_0 \frac{8 I N}{\sqrt{125}R}
\end{equation}
und einer Windungszahl $N=20$ sowie Radius $R=11,735$ ergibt sich ein Magnetfeld von
\begin{equation}
B_v = \val{B_v}.
\end{equation}

\subsection{Horizontalkomponente des Erdmagnetfelds}
Die Resonanzpeaks werden bei den Strömen in Tabelle \ref{tbl:resonanzen} erreicht.
Daraus ergibt sich jeweils das Magnetfeld B mit den Apparaturkonstanten
\begin{align}
N_{horizontal} &= 154 &\qquad R_{horizontal} &= \SI{15.79}{\centi\meter}\\
N_{sweep} &= 11 &\qquad R_{sweep} &= \SI{16.39}{\centi\meter}
\end{align}
\tbl{resonanzen}

Das Magnetfeld gegen die Frequenz ist in Abbildung \ref{fig:B_gegen_f} dargestellt.
\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{build/B_gegen_f.pdf}
	\caption{Magnetfeldstärke $B$ gegen die Frequenz $f$.}
	\label{fig:B_gegen_f}
\end{figure}

Die Fitparameter der linearen Funktion
\begin{equation}
B = a \cdot f + b
\end{equation}
ergeben sich zu
\begin{align}
a_1 &= \val{a_1} &\qquad b_1 &= \val{b_1}\\
a_2 &= \val{a_2} &\qquad b_2 &= \val{b_2}
\end{align}

Aus dem Mittelwert von $b_1$ und $b_2$ ergibt sich die Horizontalkomponente des Erdmagnetfelds
\begin{equation}
B_h = \val{B_h}
\end{equation}

\FloatBarrier
\subsection{Lande-Faktor und Kernspins}
Aus der Steigung $a_i$ ergibt sich mit
\begin{equation}
g_{F,i} = \frac{4\pi m_e}{e a_i}
\end{equation}
jeweils der Lande-Faktor
\begin{align}
g_{F,1} &= \val{g_F1}\\
g_{F,2} &= \val{g_F2}
\end{align}

Daraus lässt sich nach
\begin{equation}
I = J\left(\frac{g_S}{g_F}-1\right)
\end{equation}
mit $J = \sfrac{1}{2}$ und $g_S = \num{2.00232}$ der Kernspin bestimmen.
\begin{align}
I_1 &= \val{spin_I_1}\\
I_2 &= \val{spin_I_2}
\end{align}


\subsection{Isotopenverhältnis}
In Abbildung \ref{fig:100kHz} ist ein typischer Verlauf des Signals zu sehen.
Zu erkennen ist der erste Peak für $B=0$ so wie die Resonanzpeaks.
Deren Verhältnis ergibt sich zu
\begin{equation}
\frac{A_1}{A_2} = \num{0.53}
\end{equation}
Der erste Peak mit Kernspin $\sfrac{3}{2}$ entspricht $ ^{87}$Rb und hat ein natürliches Vorkommen von \SI{27.83}{\percent}.
Das andere Isotop $ ^{87}$Rb mit Kernspin $\sfrac{5}{2}$ hat eine Häufigkeit von \SI{72.17}{\percent}\cite{techniklexikon}.
Das natürliche Verhältnis ist also \num{0.39}.

\begin{figure}
	\centering
	\includegraphics[width=.8\textwidth]{media/100kHz_resonanz.pdf}
	\caption{Resonanzen bei \SI{100}{\kilo\hertz}.}
	\label{fig:100kHz}
\end{figure}


\FloatBarrier
\subsection{Quadratischer Zeeman-Effekt}
Die folgenden Abschätzungen werden gemacht.
\begin{align}
\Delta E_{Hy} &\propto 10^{-24}\\
B &\propto 10^{-3}\\
g_F &\propto 1\\
\mu_B &\propto 10^{-24}
\end{align}

Nach Formel (\ref{eqn:quad:zeeman}) ist der quadratische Term proportional zu $10^{-30}$.
Dies ist deutlich geringer als der lineare Term mit $10^{-27}$.
Der quadratische Term ist also drei Größenordnungen kleiner und muss nicht betrachtet werden.


\subsection{Zeitkonstante des Pumpvorgangs}
Der zeitliche Verlauf beim einschalten des RF-Feldes wird untersucht.
Dazu wird eine Exponentialfunktion der Form
\begin{equation}
U(t)=a\left(1-\exp\left(\frac{t}{\tau}\right)\right)+b
\end{equation}
gefittet. Die Ergebnisse sind in Abbildung \ref{fig:tau1} und \ref{fig:tau2} dargestellt.
Die Zeitkonstanten ergeben sich zu
\begin{equation}
\tau_1 = \val{tau_1} \qquad \tau_2 = \val{tau_2}
\end{equation}
\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{build/tau1.pdf}
	\caption{Zeitlicher Verlauf der Spannung nach Beginn des Pumpvorgangs für die erste Resonanz.}
	\label{fig:tau1}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{build/tau2.pdf}
	\caption{Zeitlicher Verlauf der Spannung nach Beginn des Pumpvorgangs für die zweite Resonanz.}
	\label{fig:tau2}
\end{figure}


\FloatBarrier
\subsection{Transiente Effekte}
Durch Messung der Periodendauer \ref{tbl:perioden} kann ein Verhältnis der Lande-Faktoren bestimmt werden.
\tbl{perioden}

Die Periodendauer wird mit der Hyperbelfunktion
\begin{equation}
T = a + \frac{b}{U - c}
\end{equation}
gefittet. Dabei ist $U$ die Amplitude der angelegten Spannung des RF-Feldes.
Es ergeben sich die Parameter
\begin{align}
	a_1 &= \val{hyp_a_1} &\qquad a_2 &= \val{hyp_a_2}\\
	b_1 &= \val{hyp_b_1} &\qquad b_2 &= \val{hyp_b_2}\\
	c_1 &= \val{hyp_c_1} &\qquad c_2 &= \val{hyp_c_2}
\end{align}
Der Plot ist in Abbildung \ref{fig:transient} dargestellt.
Das Verhältnis der Lande-Faktoren ergibt sich zu
\begin{equation}
	\frac{b_2}{b_1} = \val{b_ratio}.
\end{equation}
Der theoretische Wert für dieses Verhältnis ist \num{1.5}.

\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{build/transient.pdf}
	\caption{Periodendauer $T$ in Abhängigkeit von der Spannung $U$.}
	\label{fig:transient}
\end{figure}


%------------------------------------------------
\section{Diskussion}
%------------------------------------------------
Die gemessenen Lande-Faktoren und Kernspins stimmen innerhalb einer Standartabweichung mit den Theorievorhersagen überein.

Das Verhältnis der Isotope in der Probe von \num{0.53} ist leicht erhöht gegenüber dem Verhältnis \num{0.39} in natürlich vorkommendem Rubidium.
Die ist durch Anreicherung der Probe mit $ ^{87}$Rb zu erklären.

Auch aus den transienten Effekten konnte das Verhältnis der Lande-Faktoren zu \val{b_ratio} bestimmt werden und liegt auch damit im Bereich von einer Standartabweichung zum Theoriewert von \num{1.5}.

%------------------------------------------------
% LITERATURVERZEICHNIS
%------------------------------------------------
\begin{thebibliography}{xx}
\small
\bibitem{Anleitung}
	TU Dortmund,
	\textit{Versuch \Versuchsnummer~- \Versuchstitel},
	\url{http://129.217.224.2/HOMEPAGE/Anleitung_FPMa.html},
	abgerufen am \Versuchsdatum.
	
\bibitem{techniklexikon}
	Techniklexikon
	\url{http://techniklexikon.net/d/rubidium/rubidium.htm},
	abgerufen am 06.07.2017

\end{thebibliography}

\end{document}
