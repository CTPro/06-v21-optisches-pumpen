import numpy as np
import matplotlib.pyplot as plt
import latexoutput as lout
print("Using latexoutput, Version", lout.__version__)

"""comment out what you need"""
#import matplotlib as mpl
#mpl.rc('axes', {'formatter.useoffset': False, 'formatter.limits': [-5, 5]})
from scipy.optimize import curve_fit
#from scipy.stats import sem # standard error of mean
from uncertainties import ufloat, ufloat_fromstr
#import uncertainties.unumpy as unp
#from uncertainties.unumpy import (nominal_values as noms, std_devs as stds)
import scipy.constants as const
#from scipy.interpolate import interp1d
#from scipy.integrate import simps, quad, trapz

I_v = np.genfromtxt("data/data_I_vert.txt", unpack=True)
fq, I_s_1, I_h_1, I_s_2, I_h_2 = np.genfromtxt("data/data_Resonanzen.txt", unpack=True)
U, T_1, T_2 = np.genfromtxt("data/data_Relaxation.txt", unpack=True)

N_v = 20
R_v = 11.735e-2
N_h = 154
R_h = 15.79e-2
N_s = 11
R_s = 16.39e-2

def f(x, a, b):
	return a*x + b

def B(I, N, R):
	return const.mu_0 * 8 * I * N / (np.sqrt(125) * R)

B_v = B(I_v*1e-3, N_v, R_v) * 1e6
lout.latex_val(name='B_v', val=B_v, unit=r'\micro\tesla', digits=3)
lout.latex_val(name='I_v', val=I_v, unit=r'\milli\ampere')

B_1 = B(I_s_1*1e-3, N_s, R_s) + B(I_h_1*1e-3, N_h, R_h)
B_2 = B(I_s_2*1e-3, N_s, R_s) + B(I_h_2*1e-3, N_h, R_h)
B_1 *= 1e6
B_2 *= 1e6

lout.latex_table(name = 'resonanzen',
	content = [fq, I_s_1, I_h_1, B_1, I_s_2, I_h_2, B_2],
	col_title = r'$f$,$I_1$sweep,$I_1$,$B_1$,$I_2$sweep,$I_2$,$B_2$',
	col_unit = r'\kilo\hertz,\milli\ampere,\milli\ampere,\micro\tesla,\milli\ampere,\milli\ampere,\micro\tesla',
	fmt = '4.0,3.0,3.0,3.0,3.0,3.0,3.0',
	caption = 'Gemessene Ströme und daraus resultierende Magnetfelder jeweils bei den Resonanzen 1 und 2.')

x = fq
y = B_1
par_1, cov = curve_fit(f, x, y)
err = np.sqrt(np.diag(cov))

a_1 = ufloat(par_1[0],err[0])
b_1 = ufloat(par_1[1],err[1])
lout.latex_val(name='a_1', val=a_1, unit=r'\micro\tesla\per\kilo\hertz')
lout.latex_val(name='b_1', val=b_1, unit=r'\micro\tesla')

y = B_2
par_2, cov = curve_fit(f, x, y)
err = np.sqrt(np.diag(cov))

a_2 = ufloat(par_2[0],err[0])
b_2 = ufloat(par_2[1],err[1])
lout.latex_val(name='a_2', val=a_2, unit=r'\micro\tesla\per\kilo\hertz')
lout.latex_val(name='b_2', val=b_2, unit=r'\micro\tesla')

x_offset = (np.max(x)-np.min(x))*0.05
x_plot = np.linspace(np.min(x)-x_offset,np.max(x)+x_offset,100)

fig = plt.figure()

plt.plot(x,B_1,'x',label='Messdaten Resonanz 1')
plt.plot(x_plot,f(x_plot,*par_1),'-',label='Fit Resonanz 1')

plt.plot(x,B_2,'x',label='Messdaten Resonanz 2')
plt.plot(x_plot,f(x_plot,*par_2),'-',label='Fit Resonanz 2')

plt.xlabel("$f$ in kHz")
plt.ylabel(r"$B$ in $\mu$T")
plt.xlim(np.min(x_plot),np.max(x_plot))
plt.legend(loc='best')
plt.savefig("build/B_gegen_f.pdf")
plt.close()

B_h = (b_1 + b_2) / 2
lout.latex_val(name='B_h', val=B_h, unit=r'\micro\tesla')

g_F1 = 4 * np.pi * const.m_e / (const.e * a_1*1e-9)
g_F2 = 4 * np.pi * const.m_e / (const.e * a_2*1e-9)
lout.latex_val(name='g_F1', val=g_F1)
lout.latex_val(name='g_F2', val=g_F2)

# Kernspins ?
g_J = 2.00232
spin_I_1 = 0.5 * (g_J / g_F1 - 1)
spin_I_2 = 0.5 * (g_J / g_F2 - 1)
lout.latex_val(name='spin_I_1', val=spin_I_1)
lout.latex_val(name='spin_I_2', val=spin_I_2)


# Exponentialfit
def A(t, tau, a, b):
	return a * (1 - np.exp(-t/tau)) + b

t_1, A_1 = np.genfromtxt("data/tau1.txt", unpack=True)
t_2, A_2 = np.genfromtxt("data/tau2.txt", unpack=True)
t_2 = t_2 - t_2[0]

x = t_1
par, cov = curve_fit(A, t_1, A_1)
err = np.sqrt(np.diag(cov))

tau_1 = ufloat(par[0],err[0])
lout.latex_val(name='tau_1', val=tau_1*1e3, unit=r'\milli\second')

x_offset = (np.max(x)-np.min(x))*0.05
x_plot = np.linspace(np.min(x)-x_offset,np.max(x)+x_offset,100)

fig = plt.figure()

plt.plot(t_1,A_1,'x',label='Messdaten Resonanz 1')
plt.plot(x_plot,A(x_plot,*par),'-',label='Fit Resonanz 1')

plt.xlabel("$t$ in s")
plt.ylabel("$U$ in V")
plt.xlim(np.min(x_plot),np.max(x_plot))
plt.ylim(A_1[0], plt.ylim()[1])
plt.legend(loc='best')
plt.savefig("build/tau1.pdf")
plt.close()


x = t_2
par, cov = curve_fit(A, t_2, A_2, p0=(9e-3, 20, -14))
err = np.sqrt(np.diag(cov))

tau_2 = ufloat(par[0],err[0])
lout.latex_val(name='tau_2', val=tau_2*1e3, unit=r'\milli\second')

x_offset = (np.max(x)-np.min(x))*0.05
x_plot = np.linspace(np.min(x)-x_offset,np.max(x)+x_offset,100)

fig = plt.figure()

plt.plot(t_2,A_2,'x',label='Messdaten Resonanz 2')
plt.plot(x_plot,A(x_plot,*par),'-',label='Fit Resonanz 2')

plt.xlabel("$t$ in s")
plt.ylabel("$U$ in V")
plt.xlim(np.min(x_plot),np.max(x_plot))
plt.ylim(A_2[0], plt.ylim()[1])
plt.legend(loc='best')
plt.savefig("build/tau2.pdf")
plt.close()



# Transiente Effekte
def T(U, a, b, c):
	return a + b / (U - c)

lout.latex_table(name = 'perioden',
	content = [U, T_1, T_2],
	col_title = r'$U$,$T_1$,$T_2$',
	col_unit = r'\volt,\micro\second,\micro\second',
	fmt = '1.1,4.0,4.0',
	caption = 'Gemessene Perioden zu verschiedenen Spannungen jeweils bei den Resonanzen 1 und 2.')

x = U
y = T_1
par_1, cov = curve_fit(T, x, y, p0=(60, 770, 0.05))
err = np.sqrt(np.diag(cov))

a_1 = ufloat(par_1[0],err[0])
b_1 = ufloat(par_1[1],err[1])
c_1 = ufloat(par_1[2],err[2])
lout.latex_val(name='hyp_a_1', val=a_1)
lout.latex_val(name='hyp_b_1', val=b_1)
lout.latex_val(name='hyp_c_1', val=c_1)

y = T_2
par_2, cov = curve_fit(T, x, y, p0=(100, 1000, 0.1))
err = np.sqrt(np.diag(cov))

a_2 = ufloat(par_2[0],err[0])
b_2 = ufloat(par_2[1],err[1])
c_2 = ufloat(par_2[2],err[2])
lout.latex_val(name='hyp_a_2', val=a_2)
lout.latex_val(name='hyp_b_2', val=b_2)
lout.latex_val(name='hyp_c_2', val=c_2)

x_offset = (np.max(x)-np.min(x))*0.05
x_plot = np.linspace(np.min(x)-x_offset,np.max(x)+x_offset,100)

fig = plt.figure()

plt.plot(x,T_1,'x',label='Messdaten Resonanz 1')
plt.plot(x_plot,T(x_plot,*par_1),'-',label='Fit Resonanz 1')

plt.plot(x,T_2,'x',label='Messdaten Resonanz 2')
plt.plot(x_plot,T(x_plot,*par_2),'-',label='Fit Resonanz 2')

plt.xlabel("$U$ in V")
plt.ylabel(r"T in $\mu$s")
plt.xlim(np.min(x_plot),np.max(x_plot))
plt.legend(loc='best')
plt.savefig("build/transient.pdf")
plt.close()

b_ratio = b_2 / b_1
lout.latex_val(name='b_ratio', val=b_ratio)